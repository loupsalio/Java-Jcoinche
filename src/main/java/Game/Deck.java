package Game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
	private List<Card>	_card;
	private int				_nb_cards = 32;
	public Deck(){
		_card = new ArrayList<Card>();
		for (int a = 7; a <= 14; a++) 
			_card.add(new Card(a, color.Carreau));
		for (int a = 7; a <= 14; a++) 
			_card.add(new Card(a, color.Coeur));
		for (int a = 7; a <= 14; a++) 
			_card.add(new Card(a, color.Trefle));
		for (int a = 7; a <= 14; a++) 
			_card.add(new Card(a, color.Pique));
		Collections.shuffle(_card);
	};

	public void	show() {
		for(int i = 0; i < _card.size(); i++) {
			System.out.println("[ " + _card.get(i).get_value() + " de " + _card.get(i).get_color() + " ]");
		}
	}
	
	public Card get_card() {
		Card tmp = _card.get(0);
		_card.remove(0);
		_nb_cards--;
		
		return tmp;
	}
	
	public int get_nb_cards() {
		return _nb_cards;
	}
	
}
