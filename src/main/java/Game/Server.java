package Game;

import java.util.ArrayList;
import java.util.List;

public class Server {

	List<Room>	_free_rooms;
	List<Room>	_full_rooms;
	List<Player> _waiting_room;
	int			_nb_free_rooms = 4;
	
	Server(){
		_free_rooms = new ArrayList<Room>();
		_full_rooms = new ArrayList<Room>();
		_waiting_room = new ArrayList<Player>();
		for(int i = 0; i < _nb_free_rooms; i++)
			_free_rooms.add(new Room());
	}

	void	run() {
		int i = 0;
		while (i < 10) {
			if (_waiting_room.size() != 0 && _free_rooms.size() != 0) {
				_free_rooms.get(0).join(_waiting_room.get(0));
				if (_free_rooms.get(0).get_nb_player() == 4) {
					_full_rooms.add(_free_rooms.get(0));
					_free_rooms.get(0).run_game();
					_free_rooms.remove(0);
				}
				_waiting_room.remove(0);
			}
			try{
				Thread.sleep(500);
				}
			catch(InterruptedException e){
				System.out.println(e);
				}  
			i++;
		}
	}
	
	void	join(int socket, String name) {
		_waiting_room.add(new Player(socket, name));
	}
	
}
