package Game;

import java.util.ArrayList;
import java.util.List;

public class Room {
	int		_nb_users = 0;
	int		_score_t1 = 0;
	int		_score_t2 = 0;
	color 	_atout;
	String	_player_answer;
	List<Card>		_main;
	List<Player>	_users;
	Deck			_deck;
	
	public Room(){
		_users = new ArrayList<Player>();
		_deck = new Deck();
	}
	
	public Room	join(Player	new_user) {
		if (_nb_users == 4)
			return null;
		_users.add(new_user);
		_nb_users++;
		return this;
	}

	private int		check_players(){
		for(int i = 0; i < 4; i++) {
				if (_users.get(i).get_nb_cards() != 0)
					return -1;
		}
		return 0;
	}

	void 	round(){
		int f = 0;
		List<Card>	tas = new ArrayList<Card>();
		for(int i = 0; i < 4; i++) {
			int j = Integer.parseInt(_users.get(i).read("0"));
			if (j > 4){
				j = 1;
			}
			tas.add(_users.get(i).give_card(j));
		}
		if (f == 0){
			_score_t1++;
		}
		else
			_score_t2++;
	}

	public void	run_game() {
		if (_nb_users != 4) {
			System.out.println("Need more players");
			return;
		}
		init_cards();
		if (base_round() == 1)
			return;
		finish_cards();
		while (check_players() != 0){
			round();
		}
		if (_score_t1 > _score_t2)
			System.out.println("Team 1 won");
		else
			System.out.println("Team 2 won");
	}
	
	int	base_round(){
		Card check_atout = _deck.get_card();
		
		System.out.println("Card : " + check_atout.get_value() + " de " + check_atout.get_color());
		System.out.println("Do you want " + check_atout.get_color() + " as atout color ?");
		for (int i = 0; i < 4; i++) {
			ask(_users.get(i));
			if (_player_answer.equals("yes")) {
				System.out.println(_users.get(i).get_name() + " chose " + check_atout.get_color() + " as atout !");
				_atout = check_atout.get_r_color();
				_users.get(i).get_card(check_atout);
				return 0;
			}
		}
		System.out.println("No one want this card? hooooo !");
		return 1;
	}
	
	void	init_cards() {
		for(int i = 0; i < 4; i++) {
			for(int o = 0; o < 5; o++) {			
				_users.get(i).get_card(_deck.get_card());
			}
		}
	}
	
	void	finish_cards() {
		for(int i = 0; i < 4; i++) {
			for(int o = _users.get(i).get_nb_cards(); o < 8; o++) {			
				_users.get(i).get_card(_deck.get_card());
			}
			_users.get(i).show_hand();
		}
	}
	
	int	ask(Player user) {
		_player_answer = user.read();
		if (_player_answer == null)
			return 1;
		return 0;
	}

	public int get_nb_player() {
		return _nb_users;
	}
	
	public int	get_nb_deck_cards() {
		return _deck.get_nb_cards();
	}
	
}
