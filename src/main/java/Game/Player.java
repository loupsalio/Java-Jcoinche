package Game;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private int	_nb_cards = 0;
	private String	_name;
	private List<Card>	_Hand;
	private int			_socket;
	
	static Card	create_card(String name_card) {
		String[] tab = name_card.split(" ");
		
		if (tab.length != 2)
		{
			System.out.println("Error Card");
			return null;
		}
		if(tab[1].equals("pique") == true)
			return new Card(Integer.parseInt(tab[0]), color.Pique);
		else if(tab[1].equals("coeur"))
			return new Card(Integer.parseInt(tab[0]), color.Coeur);
		else if(tab[1].equals("carreau"))
			return new Card(Integer.parseInt(tab[0]), color.Carreau);
		else if(tab[1].equals("trefle"))
			return new Card(Integer.parseInt(tab[0]), color.Trefle);
		return null;
	};
	
	public Card	give_card(String name_card) {
		Card	tmp = create_card(name_card);
		for(int i = 0 ; i < _Hand.size() ; i++) {
			if ((_Hand.get(i).get_color().equals(tmp.get_color()) == true)
					&& (_Hand.get(i).get_value() == tmp.get_value())){
				_Hand.remove(i);
				_nb_cards--;
				return tmp;
			}
		}
		return null;
	};

	public Card	give_card(int card) {
		Card	tmp = _Hand.get(card);
		_Hand.remove(card);
		_nb_cards--;
		return tmp;
	}

		public void 	get_card(Card card) {
		
		_Hand.add(card);
		_nb_cards++;
	};
	
	public void 	get_card(String card) {
		_Hand.add(create_card(card));
		_nb_cards++;
	};

	public void	show_hand() {
		System.out.println("User " + _name + " (" + _nb_cards + " cartes) : ");
		Card tmp;
		for(int i = 0 ; i < _Hand.size() ; i++) {
			tmp = _Hand.get(i);
			System.out.println("* " + tmp.get_value() + " de " + tmp.get_color());
		}
	}

	public String		read() {
		return	"yes";
	}

	public String		read(String input) {
		return	input;
	}

	public String get_name() {
		return _name;
	}
	
	public Player(int socket_, String name_){
		_name = name_;
		_socket = socket_;
		_Hand = new ArrayList<Card>();
	}
	
	public int get_nb_cards() {
		return _nb_cards;
	}
	
}

