import java.io.IOException;
import java.net.*;

public class Serveur
{
    public static void main(String[] zero)
    {		
	ServerSocket socket;
	try {
	    socket = new ServerSocket(8888);
	    Thread t = new Thread(new Accepter_clients(socket));
	    t.start();
	    System.out.println("Mes employers sont prêts !");	
	}
	catch (IOException e)
	    {
		e.printStackTrace();
	    }
    }
}

class Accepter_clients implements Runnable
{
    private ServerSocket socketserver;
    private Socket socket;
    private int nbrclient = 1;
    public Accepter_clients(ServerSocket s)
    {
	socketserver = s;
    }

    public void run()
    {
	try {
	    while (true)
		{
		    socket = socketserver.accept();
		    System.out.println("Le client numero "+nbrclient+" est connecter !");
		    nbrclient++;
		    socket.close();
		}   
	}
	catch (IOException e)
	    {
		e.printStackTrace();
	    }
    }
}
