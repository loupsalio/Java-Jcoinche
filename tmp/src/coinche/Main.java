package coinche;

import Game.Player;
import Game.Room;

public class Main {

	public static void main(String[] args) {
		Room	main_room = new Room();

		Player	j1 = new Player("joueur 1");
		Player	j2 = new Player("joueur 2");
		Player	j3 = new Player("joueur 3");
		Player	j4 = new Player("joueur 4");

		main_room.run_game();

		main_room.join(j1);
		main_room.join(j2);
		main_room.join(j3);
		main_room.join(j4);
		
		main_room.run_game();

	}
}
