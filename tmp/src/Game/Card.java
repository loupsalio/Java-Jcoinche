package Game;

enum	color{Pique, Carreau, Coeur, Trefle};

public class Card {
	private int		_value;
	private color	_color;
	
	Card(int value_, color color_) {
		_value = value_;
		_color = color_;
	};
	
	String get_color() {
		if (_color.equals(color.Pique))
			return "Pique";
		else if (_color.equals(color.Carreau))
			return "Carreau";	
		else if (_color.equals(color.Coeur))
			return "Coeur";	
		else if (_color.equals(color.Trefle))
			return "Trefle";	
		return "Unknow";
	}
	
	color	get_r_color() {
		return _color;
	}
	
	String		get_value() {
		if (_value == 11)
			return "Valet";
		if (_value == 12)
			return "Dame";
		if (_value == 13)
			return "Roi";
		if (_value == 14)
			return "As";
		return "" +_value;
	}
	
	int		get_r_value() {
		return _value;
	}
}
