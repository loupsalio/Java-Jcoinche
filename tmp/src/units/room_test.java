package units;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Game.Player;
import Game.Room;

class room_test {

	@Test
	void test() {
		Player p1 = new Player("player 1");
		Player p2 = new Player("player 2");
		Player p3 = new Player("player 3");
		Player p4 = new Player("player 4");
		
		Room main_room = new Room();
		
		main_room.join(p1);
		main_room.join(p2);
		main_room.join(p3);
		main_room.join(p4);

		if (p1.get_nb_cards() != 0
				|| p2.get_nb_cards() != 0
				|| p3.get_nb_cards() != 0
				|| p4.get_nb_cards() != 0)
				fail("a player have a card");
		if (main_room.get_nb_deck_cards() != 32)
			fail("Deck don't have 32 cards");
		main_room.run_game();
		if (main_room.get_nb_deck_cards() != 0)
			fail("Deck don't have 0 cards");
	}

}
